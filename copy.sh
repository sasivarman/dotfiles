#!/bin/bash
# Copy all dotfiles to current directory

echo "Copying all dotfiles to current directory... "
cp ~/.zshrc .
cp ~/.tmux.conf .
cp ~/.vimrc .
echo "done."

