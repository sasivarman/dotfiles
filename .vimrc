" Vundle required setup
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Status bar plugin
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'joshdick/onedark.vim'
Plugin 'fatih/vim-go' " Go support

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required
" see :h vundle for more details or wiki for FAQ
" Put your non-Plugin stuff after this line

" Airline customization
let g:airline_powerline_fonts = 1
let g:airline_theme='onedark'

" Theme settings
syntax on
colorscheme onedark
hi Normal ctermbg=NONE

" Enable line number
set number

" Enable mouse
set mouse:a
set ttymouse=xterm2 " mouse inside tmux

" Insert space for tab
set expandtab
set tabstop=2 " number of spaces for tab key
set shiftwidth=2 " number of spaces for indentation
set softtabstop=2 " makes the spaces feel like real tabsi

" Highlight current line only in insert mode
autocmd InsertEnter * set cul
autocmd InsertLeave * set nocul

" Make backspace act normal
set backspace=indent,eol,start

" Make movement and arrow keys wrap around lines
set whichwrap+=<,>,h,l,[,]

" Use the system clipboard
set clipboard=unnamed

" Set escape key timeouts to 0 - solves ESC key lag
set timeoutlen=1000 ttimeoutlen=0

" show nicer file autocomplete menu
set wildmenu

" highlight search matches
set incsearch
set hlsearch
